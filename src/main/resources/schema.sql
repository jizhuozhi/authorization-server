CREATE TABLE IF NOT EXISTS `users`
(
    `username` varchar(255) NOT NULL,
    `password` varchar(255) NOT NULL,
    `enabled`  tinyint(4)   NOT NULL DEFAULT '0',
    PRIMARY KEY (`username`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE IF NOT EXISTS `authorities`
(
    `username`  varchar(255) NOT NULL,
    `authority` varchar(255) NOT NULL,
    PRIMARY KEY (`username`, `authority`),
    CONSTRAINT `fk_authorities_users` FOREIGN KEY (`username`) REFERENCES `users` (`username`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE IF NOT EXISTS `groups`
(
    `id`         bigint(20)  NOT NULL AUTO_INCREMENT,
    `group_name` varchar(64) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `unique_groups_group_name` (`group_name`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE IF NOT EXISTS `group_members`
(
    `username` varchar(255) NOT NULL,
    `group_id` bigint(20)   NOT NULL,
    PRIMARY KEY (`username`, `group_id`),
    KEY `fk_group_members_group_idx` (`group_id`),
    KEY `fk_group_members_users_idx` (`username`),
    CONSTRAINT `fk_group_members_group` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `fk_group_members_users` FOREIGN KEY (`username`) REFERENCES `users` (`username`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE IF NOT EXISTS `group_authorities`
(
    `group_id`  bigint(20)   NOT NULL,
    `authority` varchar(255) NOT NULL,
    PRIMARY KEY (`group_id`, `authority`),
    CONSTRAINT `fk_group_authorities_group` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE IF NOT EXISTS `oauth_client_details`
(
    `client_id`               varchar(255) NOT NULL,
    `resource_ids`            varchar(255)  DEFAULT NULL,
    `client_secret`           varchar(255)  DEFAULT NULL,
    `scope`                   varchar(255)  DEFAULT NULL,
    `authorized_grant_types`  varchar(255)  DEFAULT NULL,
    `web_server_redirect_uri` varchar(255)  DEFAULT NULL,
    `authorities`             varchar(255)  DEFAULT NULL,
    `access_token_validity`   int(11)       DEFAULT NULL,
    `refresh_token_validity`  int(11)       DEFAULT NULL,
    `additional_information`  varchar(4096) DEFAULT NULL,
    `autoapprove`             varchar(255)  DEFAULT NULL,
    PRIMARY KEY (`client_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

CREATE TABLE IF NOT EXISTS `oauth_access_token`
(
    `token_id`          varchar(255) DEFAULT NULL,
    `token`             blob,
    `authentication_id` varchar(255) DEFAULT NULL,
    `user_name`         varchar(255) DEFAULT NULL,
    `client_id`         varchar(255) DEFAULT NULL,
    `authentication`    blob,
    `refresh_token`     varchar(255) DEFAULT NULL,
    KEY `fk_oauth_access_token_users_idx` (`user_name`),
    KEY `fk_oauth_access_token_oauth_client_details_idx` (`client_id`),
    CONSTRAINT `fk_oauth_access_token_oauth_client_details` FOREIGN KEY (`client_id`) REFERENCES `oauth_client_details` (`client_id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `fk_oauth_access_token_users` FOREIGN KEY (`user_name`) REFERENCES `users` (`username`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;
