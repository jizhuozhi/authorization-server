package cn.mgdream.authorization.properties;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.util.StringUtils;

import java.util.UUID;

@ConfigurationProperties(prefix = "jwt.access.token")
@Slf4j
public class JwtAccessTokenProperties {

    private String signalKey;

    public String getSignalKey() {
        if (!StringUtils.hasText(signalKey)) {
            signalKey = UUID.randomUUID().toString();
            log.warn("Using generated signal key {}", signalKey);
        }
        return signalKey;
    }

    public void setSignalKey(String signalKey) {
        this.signalKey = signalKey;
    }
}
