package cn.mgdream.authorization.configuration;

import cn.mgdream.authorization.properties.JwtAccessTokenProperties;
import org.springframework.boot.autoconfigure.security.oauth2.authserver.AuthorizationServerProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

@Configuration
@EnableConfigurationProperties(JwtAccessTokenProperties.class)
public class JwtAccessTokenConverterConfiguration {

    @Bean
    @Primary
    public JwtAccessTokenConverter jwtAccessTokenConverter(JwtAccessTokenProperties jwtAccessTokenProperties) {
        JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();
        jwtAccessTokenConverter.setSigningKey(jwtAccessTokenProperties.getSignalKey());
        return jwtAccessTokenConverter;
    }
}
