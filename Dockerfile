FROM openjdk:11

COPY target/ /app/

WORKDIR /app/

EXPOSE 8080

ENTRYPOINT [ "java", "-jar", "authorization-server.jar" ]
